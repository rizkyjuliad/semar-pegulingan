package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class sejarah extends AppCompatActivity {
    SoundPool sp1;
    private int player1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_sejarah);

        sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player1 = sp1.load(this, R.raw.kantil5, 0);
    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(sejarah.this,
                MainActivity.class);
        startActivity(intentKu);
        sp1.play(player1, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp1.release();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}