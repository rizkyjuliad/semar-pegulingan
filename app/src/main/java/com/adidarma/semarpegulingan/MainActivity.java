package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    SoundPool sp1, sp2, sp3, sp4, sp5, sp6, sp7;

    private int player1, player2, player3, player4, player5, player6, player7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player1 = sp1.load(this, R.raw.gangsa4, 0);

        sp2 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player2 = sp2.load(this, R.raw.terompong1, 0);

        sp3 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player3 = sp3.load(this, R.raw.jegog1, 0);

        sp4 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player4 = sp4.load(this, R.raw.gangsa4, 0);

        sp5 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player5 = sp5.load(this, R.raw.jublag7, 0);

        sp6 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player6 = sp6.load(this, R.raw.gangsa3, 0);

        sp7 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player7 = sp7.load(this, R.raw.gangsa1, 0);

        ImageButton btn_instrumen = findViewById(R.id.btn_instrumen);
        btn_instrumen.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(MainActivity.this,
                        instrumen.class);
                sp1.play(player1, 1, 1, 0, 0, 1);
                startActivity(intentKu);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_sejarah = findViewById(R.id.btn_sejarah);
        btn_sejarah.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(MainActivity.this,
                        sejarah.class);
                sp2.play(player2, 1, 1, 0, 0, 1);
                startActivity(intentKu);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_video = findViewById(R.id.btn_video);
        btn_video.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(MainActivity.this,
                        video.class);
                sp3.play(player3, 1, 1, 0, 0, 1);
                startActivity(intentKu);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_bantuan = findViewById(R.id.btn_bantuan);
        btn_bantuan.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(MainActivity.this,
                        bantuan.class);
                sp4.play(player4, 1, 1, 0, 0, 1);
                startActivity(intentKu);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_tentang_kami = findViewById(R.id.btn_tentang_kami);
        btn_tentang_kami.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(MainActivity.this,
                        tentang_kami.class);
                sp5.play(player5, 1, 1, 0, 0, 1);
                startActivity(intentKu);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
        finish();
        sp6.play(player6, 1, 1, 0, 0, 1);
    }

    public void exit (View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        sp7.play(player7, 1, 1, 0, 0, 1);
        // Setting Alert Dialog Pesan

        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Dari Aplikasi?");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface arg0, int arg1) {


                sp6.play(player6, 1, 1, 0, 0, 1);

                moveTaskToBack(true);
            }
        });

        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {


                sp3.play(player3, 1, 1, 0, 0, 1);

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp7.release();
        sp6.release();
        sp5.release();
        sp4.release();
        sp3.release();
        sp2.release();
        sp1.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}