package com.adidarma.semarpegulingan;

import static java.lang.Integer.valueOf;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class instrumen_jegog extends Activity implements SeekBar.OnSeekBarChangeListener {
    Button play, stop;
    Dialog dia;
    ImageButton btnListmenu;

    private ImageButton jegog1, jegog2, jegog3, jegog4, jegog5, jegog6, jegog7;
    SeekBar volume;
    AudioManager audio;
    SoundPool sp;
    private int player1, player2, player3, player4, player5, player6, player7;

    MediaPlayer mediaPlayer;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_instrumen_jegog);
        stop = (Button) findViewById(R.id.btnstop);
        play = (Button) findViewById(R.id.btnplay);

        final MediaPlayer mp = MediaPlayer.create(instrumen_jegog.this, R.raw.tune);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.pause();
                mp.seekTo(0);
                play.setBackgroundResource(R.drawable.btn_play_gabung);
                {
                    Toast.makeText(getApplicationContext(), "MUSIK BERHENTI", Toast.LENGTH_LONG).show();
                }
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp.isPlaying()) {
                    mp.pause();
                    play.setBackgroundResource(R.drawable.btn_play_gabung);
                    {
                        Toast.makeText(getApplicationContext(), "MUSIK DIJEDA", Toast.LENGTH_LONG).show();
                    }
                } else {
                    mp.start();
                    play.setBackgroundResource(R.drawable.btn_pause_gabung);
                    {
                        Toast.makeText(getApplicationContext(), "MUSIK DIPUTAR", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play.setBackgroundResource(R.drawable.btn_play_gabung);
            }
        });
        btnListmenu = findViewById(R.id.btnlistmenu);
        btnListmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstrumentDialog();
                sp.play(player1, 1, 1, 0, 0, 1);
            }
        });

        volume = (SeekBar) findViewById(R.id.seekBar);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxvolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currentvolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        volume.setMax(maxvolume);
        volume.setProgress(currentvolume);
        volume.setOnSeekBarChangeListener(this);

        //Eksekusi Menu Dialog
        btnListmenu = findViewById(R.id.btnlistmenu);
        btnListmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstrumentDialog();entDialog();
                sp.play(player1, 1, 1, 0, 0, 1);
            }
        });

        sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);

        player1 = sp.load(this, R.raw.jegog1, 0);
        player2 = sp.load(this, R.raw.jegog2, 0);
        player3 = sp.load(this, R.raw.jegog3, 0);

        player4 = sp.load(this, R.raw.jegog4, 0);
        player5 = sp.load(this, R.raw.jegog5, 0);

        player6 = sp.load(this, R.raw.jegog6, 0);
        player7 = sp.load(this, R.raw.jegog7, 0);

        jegog1 = (ImageButton) this.findViewById(R.id.jegog1);
        jegog1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player1, 1, 1, 0, 0, 1);

                }
                return false;
            }
        });

        jegog2 = (ImageButton) this.findViewById(R.id.jegog2);
        jegog2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player2, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        jegog3 = (ImageButton) this.findViewById(R.id.jegog3);
        jegog3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player3, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        jegog4 = (ImageButton) this.findViewById(R.id.jegog4);
        jegog4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player4, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        jegog5 = (ImageButton) this.findViewById(R.id.jegog5);
        jegog5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player5, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });


        jegog6 = (ImageButton) this.findViewById(R.id.jegog6);
        jegog6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player6, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        jegog7 = (ImageButton) this.findViewById(R.id.jegog7);
        jegog7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player7, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });
    }
    private void entDialog() {
    }

    public void InstrumentDialog() {
        dia = new Dialog(instrumen_jegog.this);
        dia.setContentView(R.layout.dialog);
        dia.setCancelable(true);
        dia.show();

        TextView home = dia.findViewById(R.id.txtmenu);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                sp.play(player5, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        });

        TextView videotingklik = dia.findViewById(R.id.txtinstrument);
        videotingklik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), instrumen.class);
                startActivity(i);
                sp.play(player6, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        TextView info = dia.findViewById(R.id.txttentang);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), info_saih_pitu_5.class);
                startActivity(i);
                sp.play(player7, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    public void keluar(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        sp.play(player2, 1, 1, 0, 0, 1);
        // Setting Alert Dialog Pesan
        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Dari Aplikasi?");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                sp.play(player7, 1, 1, 0, 0, 1);
                moveTaskToBack(true);
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sp.play(player3, 1, 1, 0, 0, 1);

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(instrumen_jegog.this,
                instrumen.class);
        startActivity(intentKu);
        sp.play(player2, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.release();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, valueOf(arg1), 0);

    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {

    }
}