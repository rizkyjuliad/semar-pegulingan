package com.adidarma.semarpegulingan;

import static java.lang.Integer.valueOf;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class instrumen_trompong extends Activity implements SeekBar.OnSeekBarChangeListener {
    Button play, stop;
    Dialog dia;
    ImageButton btnListmenu;

    private ImageButton trompong1, trompong2, trompong3, trompong4, trompong5, trompong6, trompong7,
            trompong8, trompong9, trompong10, trompong11, trompong12, trompong13, trompong14;

    SeekBar volume;
    AudioManager audio;
    SoundPool sp;
    private int player1, player2, player3, player4, player5, player6, player7,
            player8, player9, player10, player11, player12, player13, player14;

    MediaPlayer mediaPlayer;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_instrumen_trompong);
        stop = (Button) findViewById(R.id.btnstop);
        play = (Button) findViewById(R.id.btnplay);

        final MediaPlayer mp = MediaPlayer.create(instrumen_trompong.this, R.raw.tune);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.pause();
                mp.seekTo(0);
                play.setBackgroundResource(R.drawable.btn_play_gabung);
                {
                    Toast.makeText(getApplicationContext(), "MUSIK BERHENTI", Toast.LENGTH_LONG).show();
                }
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp.isPlaying()) {
                    mp.pause();
                    play.setBackgroundResource(R.drawable.btn_play_gabung);
                    {
                        Toast.makeText(getApplicationContext(), "MUSIK DIJEDA", Toast.LENGTH_LONG).show();
                    }
                } else {
                    mp.start();
                    play.setBackgroundResource(R.drawable.btn_pause_gabung);
                    {
                        Toast.makeText(getApplicationContext(), "MUSIK DIPUTAR", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play.setBackgroundResource(R.drawable.btn_play_gabung);
            }
        });
        btnListmenu = findViewById(R.id.btnlistmenu);
        btnListmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstrumentDialog();
                sp.play(player1, 1, 1, 0, 0, 1);
            }
        });

        volume = (SeekBar) findViewById(R.id.seekBar);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxvolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currentvolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        volume.setMax(maxvolume);
        volume.setProgress(currentvolume);
        volume.setOnSeekBarChangeListener(this);

        //Eksekusi Menu Dialog
        btnListmenu = findViewById(R.id.btnlistmenu);
        btnListmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstrumentDialog();entDialog();
                sp.play(player1, 1, 1, 0, 0, 1);
            }
        });

        sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);

        player1 = sp.load(this, R.raw.terompong1, 0);
        player2 = sp.load(this, R.raw.terompong2, 0);
        player3 = sp.load(this, R.raw.terompong3, 0);
        player4 = sp.load(this, R.raw.terompong4, 0);
        player5 = sp.load(this, R.raw.terompong5, 0);
        player6 = sp.load(this, R.raw.terompong6, 0);
        player7 = sp.load(this, R.raw.terompong7, 0);

        player8 = sp.load(this, R.raw.terompong8, 0);
        player9 = sp.load(this, R.raw.terompong9, 0);
        player10 = sp.load(this, R.raw.terompong10, 0);
        player11 = sp.load(this, R.raw.terompong11, 0);
        player12 = sp.load(this, R.raw.terompong12, 0);
        player13 = sp.load(this, R.raw.terompong13, 0);
        player14 = sp.load(this, R.raw.terompong14, 0);

        trompong1 = (ImageButton) this.findViewById(R.id.trompong1);
        trompong1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player1, 1, 1, 0, 0, 1);

                }
                return false;
            }
        });

        trompong2 = (ImageButton) this.findViewById(R.id.trompong2);
        trompong2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player2, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong3 = (ImageButton) this.findViewById(R.id.trompong3);
        trompong3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player3, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong4 = (ImageButton) this.findViewById(R.id.trompong4);
        trompong4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player4, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong5 = (ImageButton) this.findViewById(R.id.trompong5);
        trompong5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player5, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });


        trompong6 = (ImageButton) this.findViewById(R.id.trompong6);
        trompong6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player6, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong7 = (ImageButton) this.findViewById(R.id.trompong7);
        trompong7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player7, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong8 = (ImageButton) this.findViewById(R.id.trompong8);
        trompong8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player8, 1, 1, 0, 0, 1);

                }
                return false;
            }
        });

        trompong9 = (ImageButton) this.findViewById(R.id.trompong9);
        trompong9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player9, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong10 = (ImageButton) this.findViewById(R.id.trompong10);
        trompong10.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player10, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong11 = (ImageButton) this.findViewById(R.id.trompong11);
        trompong11.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player11, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong12 = (ImageButton) this.findViewById(R.id.trompong12);
        trompong12.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player12, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });


        trompong13 = (ImageButton) this.findViewById(R.id.trompong13);
        trompong13.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player13, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });

        trompong14 = (ImageButton) this.findViewById(R.id.trompong14);
        trompong14.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //TODO Auro-generated method stub
                if (event.getAction() == 0) {
                    sp.play(player14, 1, 1, 0, 0, 1);
                }
                return false;
            }
        });
    }
    private void entDialog() {
    }

    public void InstrumentDialog() {
        dia = new Dialog(instrumen_trompong.this);
        dia.setContentView(R.layout.dialog);
        dia.setCancelable(true);
        dia.show();

        TextView home = dia.findViewById(R.id.txtmenu);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                sp.play(player5, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        });

        TextView videotingklik = dia.findViewById(R.id.txtinstrument);
        videotingklik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), instrumen.class);
                startActivity(i);
                sp.play(player6, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        TextView info = dia.findViewById(R.id.txttentang);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), info_saih_pitu.class);
                startActivity(i);
                sp.play(player7, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }

    public void keluar(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        sp.play(player2, 1, 1, 0, 0, 1);
        // Setting Alert Dialog Pesan
        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Dari Aplikasi?");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                sp.play(player7, 1, 1, 0, 0, 1);
                moveTaskToBack(true);
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sp.play(player3, 1, 1, 0, 0, 1);

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(instrumen_trompong.this,
                instrumen.class);
        startActivity(intentKu);
        sp.play(player2, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.release();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, valueOf(arg1), 0);

    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0) {

    }
}