package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class video_gangsa extends AppCompatActivity {
    VideoView vd;
    String tempat;
    DisplayMetrics dm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_video_gangsa);

        dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int tinggi = dm.heightPixels;
        int lebar = dm.widthPixels;

        vd = (VideoView) findViewById(R.id.video_view);
        vd.setMinimumHeight(tinggi);
        vd.setMinimumWidth(lebar);

        tempat = "android.resource://" + getPackageName() + "/" + R.raw.video_gangsa_fix;
        vd.setMediaController(new MediaController(this));
        vd.setVideoURI(Uri.parse(tempat));
        vd.start();
        vd.requestFocus();
    }
    public void onBackPressed() {

        super.onBackPressed();
        Intent intentKu = new Intent(video_gangsa.this,
                video.class);
        startActivity(intentKu);
        {
            Toast.makeText(getApplicationContext(), "BACK", Toast.LENGTH_LONG).show();
        }
        finish();
    }
}