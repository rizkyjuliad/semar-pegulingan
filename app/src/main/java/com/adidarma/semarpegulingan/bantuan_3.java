package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class bantuan_3 extends AppCompatActivity {
    SoundPool sp1, sp2;

    private int player1, player2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_bantuan3);

        sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player1 = sp1.load(this, R.raw.jegog5, 0);

        sp2 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player2 = sp2.load(this, R.raw.jegog2, 0);


        ImageButton home = findViewById(R.id.back);
        home.setOnClickListener(new View.OnClickListener(){
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(bantuan_3.this,
                        bantuan_2.class);
                startActivity(intentKu);

                sp2.release();
                sp1.play(player1, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(bantuan_3.this,
                        bantuan_4.class);
                startActivity(intentKu);
                sp1.release();
                sp2.play(player2, 1, 1, 0, 0, 1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(bantuan_3.this,
                bantuan_2.class);
        startActivity(intentKu);

        sp2.release();
        sp1.play(player1, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp2.release();
        sp1.release();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

}