package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class instrumen extends AppCompatActivity {
    SoundPool sp1, sp2, sp3, sp4, sp5, sp6;

    private int player1, player2, player3, player4, player5, player6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_instrumen);

        sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player1 = sp1.load(this, R.raw.terompong2, 0);

        sp2 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player2 = sp2.load(this, R.raw.kantil3, 0);

        sp3 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player3 = sp3.load(this, R.raw.gangsa3, 0);

        sp4 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player4 = sp4.load(this, R.raw.jublag2, 0);

        sp5 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player5 = sp5.load(this, R.raw.jegog3, 0);

        sp6 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player6 = sp6.load(this, R.raw.kantil1, 0);

        ImageButton btn_instrumen_trompong = findViewById(R.id.btn_instrumen_trompong);
        btn_instrumen_trompong.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        instrumen_trompong.class);

                sp1.play(player1, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_instrumen_kantil = findViewById(R.id.btn_instrumen_kantil);
        btn_instrumen_kantil.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        instrumen_kantil.class);

                sp2.play(player2, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_instrumen_gangsa = findViewById(R.id.btn_instrumen_gangsa);
        btn_instrumen_gangsa.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        instrumen_gangsa.class);

                sp3.play(player3, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_instrumen_jublag = findViewById(R.id.btn_instrumen_jublag);
        btn_instrumen_jublag.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        instrumen_jublag.class);

                sp4.play(player4, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_instrumen_jegog = findViewById(R.id.btn_instrumen_jegog);
        btn_instrumen_jegog.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        instrumen_jegog.class);

                sp5.play(player5, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        ImageButton btn_kembali = findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(instrumen.this,
                        MainActivity.class);

                sp6.play(player6, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(instrumen.this,
                MainActivity.class);
        startActivity(intentKu);
        sp2.play(player2, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp2.release();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }
}