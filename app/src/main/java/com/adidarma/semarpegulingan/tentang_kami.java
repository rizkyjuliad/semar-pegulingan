package com.adidarma.semarpegulingan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class tentang_kami extends AppCompatActivity {
    SoundPool sp1;
    private int player1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_tentang_kami);

        sp1 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        player1 = sp1.load(this, R.raw.jegog3, 0);

        ImageButton btn_kembali = findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(new View.OnClickListener() {
            public void
            onClick(View arg0) {
                Intent intentKu = new Intent(tentang_kami.this,
                        MainActivity.class);

                sp1.play(player1, 1, 1, 0, 0, 1);
                startActivity(intentKu);

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        });
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentKu = new Intent(tentang_kami.this,
                MainActivity.class);
        startActivity(intentKu);
        sp1.play(player1, 1, 1, 0, 0, 1);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp1.release();

    }


    @Override
    protected void onPause() {
        super.onPause();


    }
    @Override
    protected void onResume() {
        super.onResume();

    }
}